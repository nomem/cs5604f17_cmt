#!/bin/bash
if [ $# -ne 1 ]; then
	echo "command: run.sh scala/file/path"
	exit 1
fi
FILE_PATH=$1

JAVA_HOME=/usr/java/jdk1.8.0_151/ spark-shell --master local[*] --jars ./library/ejml-0.23.jar,library/jollyday.jar,library/protobuf.jar,library/stanford-corenlp-3.6.0-models.jar,library/stanford-english-corenlp-2016-01-10-models.jar,library/stanford-corenlp-3.6.0.jar,library/spark-corenlp-0.1.jar,library/target/scala-2.10/dlrl-library_2.10-1.0.jar,library/json4s.jar --driver-memory 5G -i $FILE_PATH