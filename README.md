# cs5604f17_cmt
This is the repository for CS5604: Information Storage And Retrieval from Fall 2017. This repository contains all the code for formatting and cleaning data.

# Dependency
The code was run in following configuration. It should work with newer configuration but watch out for the protobuf in newer version.
    
* JDK == 1.8 [link](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* Spark == 1.6 
* Scala == 2.10
* Stanford CoreNLP == 3.6 [link](https://stanfordnlp.github.io/CoreNLP/history.html)
* Stanford CoreNLP English Model == 3.6 [link](https://stanfordnlp.github.io/CoreNLP/history.html)
* json4s == 3.6 [link](http://json4s.org/)
* CoreNLP spark driver == 0.1 [link](https://spark-packages.org/package/databricks/spark-corenlp)

And a modified version of Matthew's Framework along with a list of profanity.
# How to
There are two scala files:

1. data_formatter.scala
2. clean_hbase_data.scala

And a script to run those files named `run.sh`. You also need to download the appropriate files in library folder.

**run_scala_file_with_corenlp_matthew.sh**: To use this file to run the code several modifications are needed:

1. The `JAVA_HOME` has to be set.
2. Location to all the jar files has to be set.
3. Spark driver memory is currently set to 5g for the coreNL. But it might be necessary to add more memory.

**data_formatter.scala**: This file parses json file and puts data into hbase. To make it work first json file needs to be put inside the hdfs.
Example command to do that:

    hadoop fs -put *.json /tmp/data_SFM/
Now you modify the following variables in data_formatter.scala to the hdfs directory.

    input_base
    files
    tableName

Now you can run the scala file using:

    run.sh src/data_formatter.scala
	
**clean_hbase_data.scala**: To run this code first change the following variables
	
	profanities
	tableName
	
Note that `collectionId` doesn't do anything as Matthew's framework doesn't by default do any filtering using `collectionId`.
Now run it like:

    run.sh src/clean_hbase_data.scala
	
# Others

* Profanity: We used "profanity_en.txt" for profanity list.
* Stopword: We used the spark mllib stopword list.
* Building Matthew's framework: It contains the [sbt](http://www.scala-sbt.org/download.html) build configuration.

Repository link:
[This repository](https://bitbucket.org/nomem/cs5604f17_cmt).
	

