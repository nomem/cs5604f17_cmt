// we don't need all the import 
// refine

import org.json4s._
import org.json4s.jackson.JsonMethods._
import org.apache.spark.sql.functions._
import com.databricks.spark.corenlp.functions._
import sqlContext.implicits._
import edu.vt.dlib.api.dataStructures._
import edu.vt.dlib.api.tools.WordCounter
import edu.vt.dlib.api.tools.FeatureExtractor
import edu.vt.dlib.api.tools.LDAWrapper
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapred.TableOutputFormat
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.mapred.JobConf
import org.apache.hadoop.conf.Configuration
import scala.collection.JavaConversions._
// import java.util.List
import scala.collection.mutable.WrappedArray
// for the parse to work
implicit def formats = DefaultFormats

val tableName = "final_table2"

val false_ = "false"
val negative_ = "-1"
val zero = "0"
val empty = ""

def print1(data:String) = {
  // println(data)
}

def print2(data:String) = {
  // println(data)
}

def parse_json(json_string: String, collection_name: String, collection_Id: String ):(ImmutableBytesWritable, Put) = {
	
	val json_object = parse(json_string)
	val id = (json_object \ "id_str").extract[String]
	val favorite_count = (json_object \ "favorite_count").extract[String]
	val retweet_count = (json_object \ "retweet_count").extract[String]
	val text = (json_object \ "full_text").extract[String]
	val source = (json_object \ "source").extract[String]
	var in_reply_to_user_id_str = (json_object \ "in_reply_to_user_id_str").extract[String]
	val user_id = ((json_object \ "user") \ "id_str").extract[String]
	val utc_offset = ((json_object \ "user") \ "utc_offset").extract[String]
	val profile_image_url = ((json_object \ "user") \ "profile_image_url").extract[String]
	val user_screen_name = ((json_object \ "user") \ "screen_name").extract[String]
	val user_name = ((json_object \ "user") \ "name").extract[String]
	val contributors_enabled = ((json_object \ "user") \ "contributors_enabled").extract[Boolean]
	val lang = (json_object \ "lang").extract[String]
	val created_at = (json_object \ "created_at").extract[String]
	var user_mentions_id_str = ""
	var user_mentions_name = ""
	var url = ""
	var expanded_url = ""

	if((((json_object \ "entities") \ "urls") \ "url") != JNothing){
		var dd = (((json_object \ "entities") \ "urls") \ "url")
		if (dd.isInstanceOf[JString]){
			url = dd.extract[String]
			expanded_url = (((json_object \ "entities") \ "urls") \ "expanded_url").extract[String]
		}else{
			url = dd.values.asInstanceOf[List[Map[String, String]]].mkString(";")
			expanded_url = (((json_object \ "entities") \ "urls") \ "expanded_url").values.asInstanceOf[List[Map[String, String]]].mkString(";")
		}
		
	}
	if((((json_object \ "entities") \ "user_mentions") \ "id_str") != JNothing){
		var dd = (((json_object \ "entities") \ "user_mentions") \ "id_str")
		if (dd.isInstanceOf[JString]){
			user_mentions_id_str = dd.extract[String]
			user_mentions_name = (((json_object \ "entities") \ "user_mentions") \ "name").extract[String]
		}else{
			user_mentions_id_str = dd.values.asInstanceOf[List[Map[String, String]]].mkString(";")
			user_mentions_name = (((json_object \ "entities") \ "user_mentions") \ "name").values.asInstanceOf[List[Map[String, String]]].mkString(";")
		}
		
	}
	
	var coordinates_type = "0"
	var coordinates_0 = "0" 
	var coordinates_1 = "0"
	var coordinate = ""
	if (((json_object \ "coordinates")\ "type") != JNothing ){
		coordinates_type = ((json_object \ "coordinates") \ "type").extract[String]
		val coordinate_temp = ((json_object \ "coordinates") \ "coordinates").extract[Array[String]]
		coordinates_0 = coordinate_temp(0)
		coordinates_1 = coordinate_temp(1)
		coordinate = coordinate_temp.mkString(",")
	}
	var place_country_code = ""
	var user_favourites_count = "0"
	var user_followers_count = "0"
	var user_friends_count = "0"
	var user_lang = ""
	var user_location = ""
	var user_statuses_count = "0"
	if(((json_object \ "place") \ "country_code") != JNothing){
		place_country_code = ((json_object \ "place") \ "country_code").extract[String];
	}
	if(((json_object \ "user") \ "favourites_count") != JNothing){
		user_favourites_count = ((json_object \ "user") \ "favourites_count").extract[String];
	}
	if(((json_object \ "user") \ "followers_count") != JNothing){
		user_followers_count = ((json_object \ "user") \ "followers_count").extract[String];
	}
	if(((json_object \ "user") \ "friends_count") != JNothing){
		user_friends_count = ((json_object \ "user") \ "friends_count").extract[String];
	}
	if(((json_object \ "user") \ "lang") != JNothing){
		user_lang = ((json_object \ "user") \ "lang").extract[String];
	}
	if(((json_object \ "user") \ "location") != JNothing){
		user_location = ((json_object \ "user") \ "location").extract[String];
	}
	if(((json_object \ "user") \ "statuses_count") != JNothing){
		user_statuses_count = ((json_object \ "user") \ "statuses_count").extract[String];
	}

	
	val put = new Put(Bytes.toBytes(id))
	put.add(Bytes.toBytes("metadata"),Bytes.toBytes("doc-type"), Bytes.toBytes("tweet"))
	put.add(Bytes.toBytes("metadata"),Bytes.toBytes("collection-id"), Bytes.toBytes(collection_Id))
	put.add(Bytes.toBytes("metadata"),Bytes.toBytes("collection-name"), Bytes.toBytes(collection_name))
	put.add(Bytes.toBytes("metadata"),Bytes.toBytes("dummy-data"), Bytes.toBytes(false_))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("tweet-id"), Bytes.toBytes(id))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("like-count"), Bytes.toBytes(favorite_count))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("comment-count"), Bytes.toBytes(negative_))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("retweet-count"), Bytes.toBytes(retweet_count))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("archive-source"), Bytes.toBytes("twitter-search"))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("source"), Bytes.toBytes(source))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("text"), Bytes.toBytes(text))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("screen-name"), Bytes.toBytes(user_screen_name))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user-id"), Bytes.toBytes(user_id))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("tweet-deleted"), Bytes.toBytes(false_))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user-deleted"), Bytes.toBytes(false_))
	if(contributors_enabled){
		put.add(Bytes.toBytes("tweet"),Bytes.toBytes("contributor-enabled"), Bytes.toBytes("true"))
	}else{
		put.add(Bytes.toBytes("tweet"),Bytes.toBytes("contributor-enabled"), Bytes.toBytes(false_))
	}
	
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("created-timestamp"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("created-time"), Bytes.toBytes(created_at))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("language"), Bytes.toBytes(lang))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("geo-type"), Bytes.toBytes(coordinates_type))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("geo-0"), Bytes.toBytes(coordinates_0))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("geo-1"), Bytes.toBytes(coordinates_1))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("url"), Bytes.toBytes(url))
	if(in_reply_to_user_id_str == null){
		put.add(Bytes.toBytes("tweet"),Bytes.toBytes("to-user-id"), Bytes.toBytes("null"))
	}else{
		put.add(Bytes.toBytes("tweet"),Bytes.toBytes("to-user-id"), Bytes.toBytes(in_reply_to_user_id_str))
	}
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("place_country_code"), Bytes.toBytes(place_country_code))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user-name"), Bytes.toBytes(user_name))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_favourites_count"), Bytes.toBytes(user_favourites_count))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_followers_count"), Bytes.toBytes(user_followers_count))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_friends_count"), Bytes.toBytes(user_friends_count))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_lang"), Bytes.toBytes(user_lang))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_location"), Bytes.toBytes(user_location))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_statuses_count"), Bytes.toBytes(user_statuses_count))
	
	
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_mentions_id_str"), Bytes.toBytes(user_mentions_id_str))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("user_mentions_name"), Bytes.toBytes(user_mentions_name))
	put.add(Bytes.toBytes("tweet"),Bytes.toBytes("profile-img-url"), Bytes.toBytes(profile_image_url))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean-text-solr"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean-text-cla"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean-text-cta"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("NER"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("POS"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("geo-location"), Bytes.toBytes(coordinate))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("spatial-coord"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("spatial-bounding"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("solr_gemo"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("geom-type"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("hashtags"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("mentions"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("long-url"), Bytes.toBytes(expanded_url))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("dates"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("sner-people"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("sner-organizations"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("sner-locations"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("tweet-importance"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("url_visited_cmw"), Bytes.toBytes(empty))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean_tokens"), Bytes.toBytes(empty))
	
	
	return (new ImmutableBytesWritable(Bytes.toBytes(id)), put)
}

val input_base = "/tmp/data_SFM/las_vegas_shooting/"
// "#August21","#Eclipse2017","#eclipseglasses","#Eclipse","#oreclipse","#solareclipse2017","#solareclipse",
val files = List("#LasVegas_#shooting","#VegasShooting")


for (collection_name <- files){
	val input_filename = input_base + collection_name + ".json"
	var col_name = collection_name
	var collection_Id = "1025"
	if(collection_name == "#LasVegas_#shooting"){
		col_name = "#shooting #LasVegas"
		collection_Id = "1024"
	}
	
	@transient val hadoopConf = new Configuration()
	@transient val conf = HBaseConfiguration.create(hadoopConf)
	conf.set(TableOutputFormat.OUTPUT_TABLE, tableName)
	@transient val jobConfig: JobConf = new JobConf(conf, this.getClass)
	jobConfig.setOutputFormat(classOf[TableOutputFormat])
	jobConfig.set(TableOutputFormat.OUTPUT_TABLE, tableName)

	sc.textFile(input_filename).map(l => parse_json(l,col_name,collection_Id)).saveAsHadoopDataset(jobConfig)

}

exit