import org.apache.spark.sql.functions._
import com.databricks.spark.corenlp.functions._
import sqlContext.implicits._
import edu.vt.dlib.api.dataStructures._
import edu.vt.dlib.api.tools.WordCounter
import edu.vt.dlib.api.tools.FeatureExtractor
import edu.vt.dlib.api.tools.LDAWrapper
import org.apache.hadoop.hbase.HBaseConfiguration
import org.apache.hadoop.hbase.client.Put
import org.apache.hadoop.hbase.io.ImmutableBytesWritable
import org.apache.hadoop.hbase.mapred.TableOutputFormat
import org.apache.hadoop.hbase.util.Bytes
import org.apache.hadoop.mapred.JobConf
import org.apache.hadoop.conf.Configuration
import scala.collection.JavaConversions._
import java.util.List
import scala.collection.mutable.WrappedArray


val profanities: Array[String] = sc.textFile("file:/home/cloudera/Downloads/profanity_en.txt").collect()

def sanitize(tweet: HBaseTweet): HBaseTweet = {
	tweet.cleanURLs()
	tweet.cleanMentions()
	// tweet.cleanHashtags()
	tweet.cleanPunctuation()
	tweet.cleanRTMarker()
	tweet.toLowerCase()
	tweet.cleanStopWords()
	return tweet
}

def sanitize2(tweet: HBaseTweet): HBaseTweet = {
	//clean hashes
	tweet.tokens = tweet.text.filter(!"#".contains(_)).split(" ")
	tweet.cleanURLs()
	tweet.cleanMentions()
	tweet.cleanPunctuation()
	tweet.cleanRTMarker()
	tweet.toLowerCase()
	tweet.cleanStopWords()
	tweet.tokens = tweet.tokens.map(c => if(profanities.contains(c)) c(0)+c.substring(1,c.length).map(cc => '*') else c)
	return tweet
}



def convertToPut(id: String, tokens: WrappedArray[String], nerTags: WrappedArray[String], posTags: WrappedArray[String], cleandata: WrappedArray[String], hashtags: String, mentions: String, urls: String, rt: Boolean): (ImmutableBytesWritable, Put) = {
	val rowkey = id
	val put = new Put(Bytes.toBytes(rowkey))
	var people = ""
	var organization = ""
	var dates = ""
	var locations = ""
	var POS = ""
	var NER = ""
	var rtval = "false"
	if(rt){
		rtval = "true"
	}
	for ( i <- 0 to (nerTags.length - 1)) {
		if(nerTags(i) == "PERSON"){
			people = people + tokens(i) +";"
		}
		if(nerTags(i) == "ORGANIZATION"){
			organization = organization + tokens(i) +";"
		}
		if(nerTags(i) == "DATE"){
			dates = dates + tokens(i) +";"
		}
		if(nerTags(i) == "TIME"){
			dates = dates + tokens(i) +";"
		}
		if(nerTags(i) == "LOCATION"){
			locations = locations + tokens(i) +";"
		}
		if(nerTags(i) == "O"){
			NER = NER + tokens(i)+" "
		}else{
			NER = NER + "<em class=\'"+nerTags(i)+"\'>"+tokens(i)+"</em> "
		}
	}
	for ( i <- 0 to (posTags.length - 1)) {
		if(posTags(i) == ":"){
			POS = POS + tokens(i)+" "
		}else{
			POS = POS + "<em class=\'"+posTags(i)+"\'>"+tokens(i)+"</em> "
		}
	}
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean-text-solr"), Bytes.toBytes(cleandata.mkString(" ")))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean-text-cta"), Bytes.toBytes(cleandata.mkString(" ")))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean-text-cla"), Bytes.toBytes(cleandata.mkString(" ")))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("clean_tokens"), Bytes.toBytes(cleandata.mkString(";")))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("NER"), Bytes.toBytes(NER))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("POS"), Bytes.toBytes(POS))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("rt"), Bytes.toBytes(rtval))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("hashtags"), Bytes.toBytes(hashtags))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("mentions"), Bytes.toBytes(mentions))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("dates"), Bytes.toBytes(dates))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("sner-people"), Bytes.toBytes(people))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("sner-organizations"), Bytes.toBytes(organization))
	put.add(Bytes.toBytes("clean-tweet"),Bytes.toBytes("sner-locations"), Bytes.toBytes(locations))
	return (new ImmutableBytesWritable(Bytes.toBytes(rowkey)), put)
}

val tableName = "final_table2"
val textColumnFamily = "tweet"
val textColumnName = "text"
val collectionId = "997"

val hbaseconfig = new HBaseConfig()
hbaseconfig.tableName = tableName
hbaseconfig.textColumnFamily = textColumnFamily
hbaseconfig.textColumnName = textColumnName

val tweetcollectionfactory = new TweetCollectionFactory(sc,sqlContext)
val tweetcollection = tweetcollectionfactory.createFromHBase(collectionId,hbaseconfig)
tweetcollection.applyFunction(sanitize2)
val input = tweetcollection.getCollection().map(tweet => (tweet.id,tweet.text,tweet.tokens.mkString(" ")+".",tweet.mentions.mkString(","),tweet.hashtags.mkString(","),tweet.urls.mkString(","),tweet.isRetweet)).toDF("id","text","cleaned","mentions","hashtags","urls","rt")
// input.tail(5).show(numRows = 5,truncate = true)
// input_dataframe.show(numRows = 1)

// val input = Seq((1, "<xml>Stanford University is located in California. It is a great university.</xml>")).toDF("id", "text")

val output = input.select('text.as('sen),'cleaned,'id,'hashtags,'mentions,'urls,'rt).select('rt,'hashtags,'mentions,'urls,'id,'sen,lemma('cleaned).as('cleandata), tokenize('sen).as('words), ner('sen).as('nerTags), pos('sen).as('posTags))
// // output.show(truncate = true)
output.show(numRows = 5, truncate = false)

@transient val hadoopConf = new Configuration()
@transient val conf = HBaseConfiguration.create(hadoopConf)
conf.set(TableOutputFormat.OUTPUT_TABLE, hbaseconfig.tableName)
@transient val jobConfig: JobConf = new JobConf(conf, this.getClass)
jobConfig.setOutputFormat(classOf[TableOutputFormat])
jobConfig.set(TableOutputFormat.OUTPUT_TABLE, hbaseconfig.tableName)

output.map(row => convertToPut(row.getAs("id"),row.getAs[WrappedArray[String]]("words"),row.getAs[WrappedArray[String]]("nerTags"),row.getAs[WrappedArray[String]]("posTags"),row.getAs[WrappedArray[String]]("cleandata"),row.getAs("hashtags"),row.getAs("mentions"),row.getAs("urls"),row.getAs[Boolean]("rt"))).saveAsHadoopDataset(jobConfig)

exit()
